import * as moment from 'moment';
import { FETCH_SEARCH_PARAMS, ISearchParams, SearchParamsActionTypes } from './types';

const initialState: ISearchParams = {
  departurePoint: '',
  arrivalPoint: '',
  departureDate: moment(),
  arrivalDate: moment().add(1, 'd'),
};

export function fetchSearchParamsReducer(
  state = initialState,
  action: SearchParamsActionTypes
): ISearchParams {
  switch (action.type) {
    case FETCH_SEARCH_PARAMS: {
      return {
        departurePoint: action.departurePoint,
        arrivalPoint: action.arrivalPoint,
        departureDate: action.departureDate,
        arrivalDate: action.arrivalDate,
      };
    }
    default:
      return state;
  }
}
