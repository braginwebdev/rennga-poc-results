import { FETCH_SEARCH_PARAMS, ISearchParams } from './types';

export const fetchSearchParams = (searchParams: ISearchParams) => {
  return {
    type: FETCH_SEARCH_PARAMS,
    ...searchParams,
  };
};
