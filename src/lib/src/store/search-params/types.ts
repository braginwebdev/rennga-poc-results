import { Moment } from 'moment';

export const FETCH_SEARCH_PARAMS = 'FETCH_SEARCH_PARAMS';

export interface ISearchParams {
  departurePoint: string;
  arrivalPoint: string;
  departureDate: Moment;
  arrivalDate: Moment;
}

interface IFetchSearchParamsAction {
  type: typeof FETCH_SEARCH_PARAMS;
  departurePoint: string;
  arrivalPoint: string;
  departureDate: Moment;
  arrivalDate: Moment;
}

export type SearchParamsActionTypes = IFetchSearchParamsAction;
