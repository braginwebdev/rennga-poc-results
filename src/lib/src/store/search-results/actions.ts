import { hardcodedFlights } from '../../data/flights';
import { ISearchParams } from '../search-params/types';
import { FETCH_FLIGHTS } from './types';

export const searchFlights = (searchPayload: ISearchParams) => {
  const flights = hardcodedFlights.filter(flight => {
    return (
      searchPayload.departurePoint === flight.departurePoint &&
      searchPayload.arrivalPoint === flight.arrivalPoint &&
      !searchPayload.departureDate.diff(flight.departureDate, 'days') &&
      !searchPayload.arrivalDate.diff(flight.arrivalDate, 'days')
    );
  });
  return {
    type: FETCH_FLIGHTS,
    flights,
  };
};
