import { FETCH_FLIGHTS, IFlightResults, SearchActionTypes } from './types';

const initialState: IFlightResults = {
  flights: [],
};

export function searchFlightsReducer(
  state = initialState,
  action: SearchActionTypes
): IFlightResults {
  switch (action.type) {
    case FETCH_FLIGHTS: {
      return { flights: action.flights };
    }
    default:
      return state;
  }
}
