import { Moment } from 'moment';

export const FETCH_FLIGHTS = 'FETCH_FLIGHTS';

export interface IFlight {
  departurePoint: string;
  arrivalPoint: string;
  number: string;
  departureDate: Moment;
  arrivalDate: Moment;
  class: string;
  freeSpots: number;
  price: number;
}

export interface IFlightResults {
  flights: IFlight[];
}

interface IFetchFlightsAction {
  type: typeof FETCH_FLIGHTS;
  flights: IFlight[];
}

export type SearchActionTypes = IFetchFlightsAction;
