import { combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { fetchSearchParamsReducer } from './search-params/reducers';
import { searchFlightsReducer } from './search-results/reducers';

const rootReducer = combineReducers({
  flightResults: searchFlightsReducer,
  searchParams: fetchSearchParamsReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
  return createStore(rootReducer, composeWithDevTools());
}
