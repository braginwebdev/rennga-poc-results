import * as React from 'react';
import { Provider } from 'react-redux';
import { Action, Store } from 'redux';
import FlightResults from './containers/FlightResults';
import configureStore from './store';
import { ISearchParams } from './store/search-params/types';

interface ISubAppProps {
  searchData: ISearchParams;
}

class SubApp extends React.Component<ISubAppProps> {
  private readonly store: Store<any, Action>;
  public constructor(props: ISubAppProps) {
    super(props);
    this.store = configureStore();
  }
  public render() {
    return (
      <Provider store={this.store}>
        <FlightResults searchData={this.props.searchData} />
      </Provider>
    );
  }
}

export default SubApp;
