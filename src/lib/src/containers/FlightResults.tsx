import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../store';
import { fetchSearchParams } from '../store/search-params/actions';
import { ISearchParams } from '../store/search-params/types';
import { searchFlights } from '../store/search-results/actions';
import { IFlightResults } from '../store/search-results/types';

interface IFlightResultsProps extends IFlightResults {
  searchData: ISearchParams;
  searchFlights: typeof searchFlights;
  fetchSearchParams: typeof fetchSearchParams;
}

class FlightResults extends React.Component<IFlightResultsProps> {
  constructor(props: IFlightResultsProps) {
    super(props);
    this.props.fetchSearchParams(props.searchData);
  }
  public componentDidMount() {
    this.props.searchFlights(this.props.searchData);
  }

  public componentDidUpdate(prevProps: IFlightResultsProps) {
    if (
      prevProps.searchData.departurePoint !== this.props.searchData.departurePoint ||
      prevProps.searchData.arrivalPoint !== this.props.searchData.arrivalPoint ||
      prevProps.searchData.departureDate.diff(this.props.searchData.departureDate, 'days') ||
      prevProps.searchData.arrivalDate.diff(this.props.searchData.arrivalDate, 'days')
    ) {
      this.props.searchFlights(this.props.searchData);
    }
  }

  public render() {
    const { flights } = this.props;
    if (!flights.length) {
      return null;
    }
    return (
      <section>
        <table>
          <thead>
            <tr>
              <th>Code</th>
              <th>Departure Time</th>
              <th>Arrival Time</th>
              <th>Available Spots</th>
              <th>Class</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {flights.map((item, key) => (
              <tr key={key}>
                <td>{item.number}</td>
                <td>{item.departureDate.format('hh:mm')}</td>
                <td>{item.arrivalDate.format('hh:mm')}</td>
                <td>{item.freeSpots}</td>
                <td>{item.class}</td>
                <td>{item.price}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </section>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  flights: state.flightResults.flights,
});

const dispatchStateToProps = (dispatch: Dispatch) => ({
  fetchSearchParams: (searchPayload: ISearchParams) => dispatch(fetchSearchParams(searchPayload)),
  searchFlights: (searchPayload: ISearchParams) => dispatch(searchFlights(searchPayload)),
});

export default connect(
  mapStateToProps,
  dispatchStateToProps
)(FlightResults);
