import * as moment from 'moment';
import * as React from 'react';
import { SubApp } from '../lib/';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <SubApp
          searchData={{
            departurePoint: 'Tokyo',
            arrivalPoint: 'Yokohama',
            departureDate: moment(),
            arrivalDate: moment().add(1, 'd'),
          }}
        />
      </div>
    );
  }
}

export default App;
