import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './examples/App';

ReactDOM.render(<App />, document.getElementById('root') as HTMLElement);
